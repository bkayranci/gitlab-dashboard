import axios from 'axios'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
Vue.config.productionTip = false
Vue.prototype.$http = axios

new Vue({
  router,
  render: h => h(App),
  data: {
    profiles: []
  },
  mounted() {
    this.readProfiles()
  },
  methods: {
    readProfiles: function () {
      let profilesRaw = localStorage.getItem("profiles");
      this.profiles = []
      if (profilesRaw != null || profilesRaw != undefined) {
        let profileParsed = JSON.parse(profilesRaw);
        if (Array.isArray(profileParsed)) {
          profileParsed.forEach((profile) => {
            this.profiles.push(profile);
          });
        }
      }
    }
  }
}).$mount('#app')
