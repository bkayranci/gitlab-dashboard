# gitlab-ci-dashboard

[![pipeline status](https://gitlab.com/bkayranci/gitlab-dashboard/badges/master/pipeline.svg)](https://gitlab.com/bkayranci/gitlab-dashboard/-/commits/master)


The project aims to visualize GitlabCI pipelines for selected projects. It do not need a database or backend service. It depends only Gitlab API.

## Installation
### Gitlab Pages

soon...

### Container

soon...


## Development
### Project setup
```
yarn install
```

#### Compiles and hot-reloads for development
```
yarn serve
```

#### Compiles and minifies for production
```
yarn build
```

#### Lints and fixes files
```
yarn lint
```
