module.exports = {
  publicPath: process.env.PUBLIC_PATH == undefined
    ? '/'
    : process.env.PUBLIC_PATH
}