FROM node:14-alpine as build

WORKDIR /build
COPY package.json .
RUN yarn install

COPY . .

RUN VUE_APP_NODE_ENV=production yarn build

FROM nginx:1.21-alpine as runtime

COPY ./.docker/nginx-server.conf.template /etc/nginx/templates/default.conf.template
ENV NGINX_PORT=8080
EXPOSE 8080
COPY --from=build /build/dist/ /usr/share/nginx/html/